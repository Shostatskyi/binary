#pragma once
#include <iostream>
#include <string>
using namespace std;

class Binary
{
	string _num;
    
    public:
		Binary():_num("") {}
		Binary(string num) :_num(num) {}
		Binary(Binary const &b) :_num(b._num) {}

		void showNum() const { cout << _num << endl; }
		string getNum() const { return _num; }

		//void add_bin();
		// the rest in process
};